package com.example.apiaomg.util

import androidx.fragment.app.Fragment
import com.example.apiaomg.App

fun Fragment.getViewModelFactory(): ViewModelFactory {
        val repository = App.newsRepository
        return ViewModelFactory(repository, this)
    }