package com.example.apiaomg

import android.app.Application
import com.example.apiaomg.data.NewsRepository
import com.example.apiaomg.data.remote.NewsRemoteDataSource

class App : Application() {

    companion object {
        lateinit var instance: App

        val newsRepository by lazy {
            NewsRepository(NewsRemoteDataSource)
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}